# JeuAngular

Le but de ce projet est de transformer le jeu fait en Java vers Angular.

# Présentation

* Rendu
* Structure
* Finalisation

## Rendu

* Choix du nom des joueurs :

<img src="Ressources/Noms.png" alt="" width="300">

* Partie en cours :

<img src="Ressources/Jeu.png" alt="" width="300">

## Structure

### App Component

Fait l'interface entre les components.

### Entities

Contient l'interface player (id, name, color)

### Token Provider

Mémoire de la position des jetons. Contient une méthode pour donner l'état de la grille et une pour rajouter un jeton.

```
Amélioration possible :
* local storage afin de mémoriser l'état d'une partie pour la reprendre plus tard.
```

### Slot Component

Correspond à un slot ou peut être positionné un jeton. Il est multiplié autant de fois qu'il faut pour créer la grille.
Ce component attent en entrée un numérique pour d'affichage de la couleur du jeton et renvoi un event au clic.

### Grid Component

Génère l'affichage de la grille, gère le positionnement des jetons et effectue les contrôles d'alignement.
Ce component attent en entrée un numérique correspondant à la couleur du joueur actuel et renvoi un event fin de tour.

### Player Component

Correspond à l'affichage de chaque joueur pour la saisie du nom puis l'affichage de celui-ci.

* Dans un premier temps, j'ai utilisé la fonction ViewChild pour sélection l'objet input.
```javascript
@ViewChild('inputName') inputName?: ElementRef<HTMLInputElement>;

public storeName(event: Event) {
    event.preventDefault();
  if (this.inputName?.nativeElement.value) {
    this.name = this.inputName?.nativeElement.value;
    this.hideForm = true;
  }
}
```
* Ensuite, conversion du formulaire en utilisant le ngModule.
* J'ai eu le soucis suivant à l'appel du component dans app.component.html.

Ce code ne marche pas :
```javascript
  *ngFor="let player of players;
```
Dès que l'on saisie le nom d'un joueur, l'affichage ne se comporte pas comme celà devrait.

Ce code marche :
```javascript
  *ngFor="let player of [].constructor(numberOfPlayer);
```

```
Amélioration possible :
* Demander un choix de couleur pour personnaliser les jetons.
```

## Finalisation

Le jeu est fonctionnel mais il manque :
* affichage du vainqueur ou non avec une modal en fin de partie (ceci est fait dans la console)
* blocage du jeu une fois la partie terminée.
* amélioration de l'affichage et de l'ergonomie
* possibilité de personnaliser la taille de la grille et pourquoi pas du nombre de joueurs.
