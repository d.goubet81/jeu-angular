import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenProviderService {

  private tokens: number[][] = Array.from({ length: 6 }, () => new Array(7).fill(0));

  constructor() { }

  getTokens(): number[][] {
    return this.tokens;
  }

  setTokens(position: number, value: number): boolean {
    let line: number = 0;
    let placed: boolean = false;
    while (line < this.tokens.length && !this.tokens[line][position]) {
      this.tokens[line][position] = value!;
      placed = true;
      if (line > 0) {
        this.tokens[line - 1][position] = 0;
      }
      line++;
    }
    return placed;
  }
}
