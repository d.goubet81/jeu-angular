import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { TokenProviderService } from '../token-provider.service';

@Component({
  selector: 'app-grid[playerColor]',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnChanges {

  lineSize: number = 0;
  colSize: number = 0;
  tokensToAlign: number = 4;

  tokens: number[][] = []

  @Input() playerColor?: number;

  @Output() turn = new EventEmitter();

  constructor(private tp: TokenProviderService) { }

  ngOnInit(): void {
    this.getTokens()
  }

  ngOnChanges(): void {
  }

  getTokens() {
    this.tokens = this.tp.getTokens();
    this.lineSize = this.tokens.length;
    this.colSize = this.tokens[0].length;
  }

  playToken(position: number) {
    if (this.playerColor) {
      if (this.tp.setTokens(position, this.playerColor)) {
        this.getTokens()
        let turnState: number = this.checkPlayerWin() ? this.playerColor : this.checkGridFull() ? -1 : 0;
        this.turn.emit(turnState);
      } else {
        alert('La colonne est pleine!');
      }
    } else {
      console.log("Il n'y a pas de joueur actif");
    }
  }

  checkPlayerWin(): boolean {
    let hasWinner: boolean = false;

    let lineIndex: number = 0;
    while (lineIndex < this.tokens.length && !hasWinner) {
      let colIndex: number = 0;
      while (colIndex < this.tokens[lineIndex].length && !hasWinner) {
        if (this.tokens[lineIndex][colIndex] == this.playerColor) {
          // Test alignement horizontal
          hasWinner = this.findAlignment(lineIndex, colIndex, 0, 1);
          if (!hasWinner) { // Test alignement vertical
            hasWinner = this.findAlignment(lineIndex, colIndex, 1, 0);
            if (!hasWinner) { // Test alignement diagonal à droite
              hasWinner = this.findAlignment(lineIndex, colIndex, 1, 1);
              if (!hasWinner) { // Test alignement diagonal à gauche
                hasWinner = this.findAlignment(lineIndex, colIndex, 1, -1);
              }
            }
          }
        }
        colIndex++;
      }
      lineIndex++;
    }
    return hasWinner;
  }

  private findAlignment(lineNumber: number, colNumber: number,
    lineShift: number, colShift: number): boolean {

    let hasWinner: boolean = false;
    let idtTokens: number = 1;

    if (this.isPossible(lineNumber, colNumber, lineShift, colShift)) {
      while (idtTokens < this.tokensToAlign && this.tokens[lineNumber + idtTokens * lineShift][colNumber
        + idtTokens * colShift] == this.playerColor) {
        idtTokens++;
      }
      if (idtTokens >= this.tokensToAlign) {
        hasWinner = true;
      }
    }
    return hasWinner;
  }

  isPossible(line: number, col: number, lineShift: number, colShift: number): boolean {
    let result: boolean = false;

    let horizontalCheck: boolean = col * colShift <= (this.tokens[0].length - this.tokensToAlign) * colShift;
    let verticalCheck: boolean = line * lineShift <= (this.tokens.length - this.tokensToAlign) * lineShift;

    if (verticalCheck && horizontalCheck) {
      result = true;
    }
    return result;
  }

  checkGridFull(): boolean {
    let gridFull: boolean = true;
    for (let col: number = 0; col < this.tokens[0].length; col++) {
      if (!this.tokens[0][col]) {
        gridFull = false;
        break;
      }
    }
    return gridFull;
  }
}
