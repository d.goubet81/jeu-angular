import { Component } from '@angular/core';
import { Player } from 'src/entities';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'jeu-angular';

  numberOfPlayer: number = 2;
  players: Player[] = Array(this.numberOfPlayer).fill({ id: 0, name: '', color: '' });
  currentPlayer: number = 1;

  nameEntered: number = 0;
  allNamesEntered: boolean = false;

  gameLineSize: number = 6;
  gameColSize: number = 7;

  checkPlayer(player: Player) {
    this.players[player.id - 1] = player;
    this.nameEntered++;
    this.allNamesEntered = (this.nameEntered == this.numberOfPlayer) ? true : false;
  }

  endTurn(state: number) {
    if (state == -1) {
      console.log("Pas de vainqueur...");
    } else {
      if (state == 0) {
        this.changePlayer();
      } else {
        console.log(`Victoire de ${this.players[state-1].name}!`);
      }
    }
  }

  changePlayer() {
    if (this.currentPlayer == 1) {
      this.currentPlayer = 2;
    } else {
      this.currentPlayer = 1;
    }
  }
}
