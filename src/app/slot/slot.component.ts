import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-slot[state]',
  templateUrl: './slot.component.html',
  styleUrls: ['./slot.component.css']
})
export class SlotComponent implements OnInit, OnChanges {

  color: string = ''

  @Input() state?: number;

  @Output() play = new EventEmitter();

  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges() {
    this.setColor(this.state);
  }

  setColor(state: number | undefined) {
    switch (this.state) {
      case 1:
        this.color = 'yellow';
        break;
      case 2:
        this.color = 'red';
        break;
      default:
        this.color = 'empty';
        break;
    }
  }

  playToken() {
    this.play.emit();
  }

}
