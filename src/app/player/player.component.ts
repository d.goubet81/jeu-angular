import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Player } from 'src/entities';

@Component({
  selector: 'app-player[id]',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  hideForm: boolean = false;

  player: Player= {
    id: 0,
    name: '',
    color: ''
  };;

  playerModel: Player = {
    id: 0,
    name: '',
    color: ''
  };


  @Input() id?: number;

  @Output() validName = new EventEmitter();

  // @ViewChild('inputName') inputName?: ElementRef<HTMLInputElement>;

  constructor() { }

  ngOnInit() {
       switch (this.id) {
      case 1:
        this.player.id = this.id;
        this.player.color = 'yellow';
        break;
      case 2:
        this.player.id = this.id;
        this.player.color = 'red';
        break;
    }
  }

  public OnSubmitPlayer(event: Event) {
    event.preventDefault();

    if (this.playerModel.name) {
      this.player.name = this.playerModel.name;
      this.hideForm = true;
      this.validName.emit(this.player);
      this.playerModel.name = '';
    }
  }

  // public storeName(event: Event) {
  //   event.preventDefault();

  //   if (this.inputName?.nativeElement.value) {
  //     this.name = this.inputName?.nativeElement.value;
  //     this.hideForm = true;
  //   }
  // }
}
